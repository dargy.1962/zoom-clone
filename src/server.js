import http from "http";
import WebSocket from "ws";
import express, { json } from "express";

// 서버에서 움직이는 애들은 터미널에 결과가 뜹니다. (사용자에게 보이는 페이지가 아님. )

const app = express();

app.set("view engine", "pug");
app.set("views", __dirname + "/views");

app.use("/public", express.static(__dirname + "/public"));

app.get("/", (req, res) => res.render("home"));
app.get("/*", (req, res) => res.redirect("/"));

const handleListen = () => console.log("Listening on http://localhost:3000");

const server = http.createServer(app);
const wss = new WebSocket.Server({ server });

const sockets = [];

wss.on("connection", (socket) => {
  // 브라우저 추가
  sockets.push(socket);
  socket["nickname"] = "익명이";

  // 연결 여부
  console.log("브라우저와 연결되었습니다. ✨");
  socket.on("close", () =>
    console.log("브라우저와의 연결이 종료되었습니다. 🎄")
  );

  // 받은 메시지 처리
  socket.on("message", (msg) => {
    const message = JSON.parse(msg);
    console.log(
      "브라우저에서 메시지가 도착했습니다. 📢 [ ",
      message.type,
      message.payload,
      " ]"
    );

    // type에 따라 처리하기
    switch (message.type) {
      case "new_message":
        sockets.forEach((aSocket) =>
          aSocket.send(`${socket.nickname} : ${message.payload}`)
        );
        break;

      case "nickname":
        socket["nickname"] = message.payload;
        break;
    }
  });
});

// 2000로 하니까 안된다!
server.listen(3000, handleListen);
